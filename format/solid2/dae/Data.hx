package format.solid2.dae;

import format.solid2.data.Vector;
import format.solid2.data.UV;

typedef Collada = {
	asset:ColladaAsset,
	images:Array<ColladaImage>,
	effects:Array<ColladaEffect>,
	materials:Array<ColladaMaterial>,
	geometries:Array<ColladaGeometry>,
	animations:Array<ColladaAnimation>,
	animation_clips:Array<ColladaAnimationClip>,
}

typedef ColladaAsset = {
	contributor:Map<String,String>,
	created:Date,
	modified:Date,
	unit:{name:String, value:Float},
	up_axis:String,
}

typedef ColladaImage = {
	name:String,
	id:String,
	init_from:String,
}

typedef ColladaEffect = {
	id:String,
	parameters:Array<ColladaEffectParameter>,
	technique:ColladaEffectTechnique,
}

typedef ColladaEffectParameter = {
	sid:String,
	type:String,
	source:String,
}

typedef ColladaEffectTechnique = {
	render:String,
	diffuse:ColladaEffectTechniqueSource,
	emission:ColladaEffectTechniqueSource,
}

enum ColladaEffectTechniqueSource
{
	color( value:Vector );
	texture( texture:String, texcoord:String );
}

typedef ColladaMaterial = {
	id:String,
	name:String,
	instance_effect:ColladaEffect,
}

typedef ColladaGeometry = {
	id:String,
	name:String,

	vertices:Array<Vector>,
	normals:Array<Vector>,
	uv:Array<UV>,
}

typedef ColladaAnimation = {
	id:String,
	target:String,

	time:Float,
	transform:Array<Float>,
	interpolation:String,
}

typedef ColladaAnimationClip = {
	
}
