package format.solid2.mtl;

import format.solid2.data.Vector;

typedef MTL = {
	materials:Map<String,MTLMaterial>
};

typedef MTLMaterial = {
	name:String,
	Ka:Vector,
	Kd:Vector,
	Ks:Vector,
	d:Float,
	illum:Int,
	map_Ka:String,
	map_Kd:String,
	map_d:String,
	map_Ks:String,
	map_bump:String,
};

