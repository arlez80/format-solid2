package format.solid2.mtl;

import format.solid2.mtl.Data;
import format.solid2.data.Vector;
import haxe.io.Eof;
import haxe.io.Input;

/**
 * .mtl Reader
 * @author あるる（きのもと 結衣）
 */
class Reader
{
	private var bi:Input;

	/**
	 * コンストラクタ
	 * @param	i
	 */
	public function new( bi:Input ) 
	{
		this.bi = bi;
	}

	/**
	 * 読み込む
	 * @return
	 */
	public function read( ):MTL
	{
		var materials = new Map<String,MTLMaterial>( );

		var name = "";
		var Ka:Vector = null;
		var Kd:Vector = null;
		var Ks:Vector = null;
		var d:Float = 1.0;
		var illum:Int = 1;
		var map_Ka = "";
		var map_Kd = "";
		var map_d = "";
		var map_Ks = "";
		var map_bump = "";

		var addGroup = function() {
			if ( name != "" ) {
				materials.set( name, {
					name: name,
					Ka: Ka,
					Kd: Kd,
					Ks: Ks,
					d: d,
					illum: illum,
					map_Ka: map_Ka,
					map_Kd: map_Kd,
					map_d: map_d,
					map_Ks: map_Ks,
					map_bump: map_bump,
				} );
			}

			name = "";
			Ka = null;
			Kd = null;
			Ks = null;
			d = 1.0;
			illum = 1;
			map_Ka = "";
			map_Kd = "";
			map_d = "";
			map_Ks = "";
			map_bump = "";
		};

		try {
			while ( true ) {
				var data = this.bi.readLine( ).split( " " );
				switch( data[0] ) {
					case "newmtl":
						addGroup( );
						name = data[1];
					case "Ka":
						Ka = new Vector( Std.parseFloat(data[1]), Std.parseFloat(data[2]), Std.parseFloat(data[3]) );
					case "Kd":
						Kd = new Vector( Std.parseFloat(data[1]), Std.parseFloat(data[2]), Std.parseFloat(data[3]) );
					case "Ks":
						Ks = new Vector( Std.parseFloat(data[1]), Std.parseFloat(data[2]), Std.parseFloat(data[3]) );
					case "d":
						d = Std.parseFloat(data[1]);
					case "illum":
						illum = Std.parseInt(data[1]);
					case "map_Ka":
						map_Ka = data[1];
					case "map_Kd":
						map_Kd = data[1];
					case "map_d":
						map_d = data[1];
					case "map_Ks":
						map_Ks = data[1];
					case "map_bump":
						map_bump = data[1];
					case "bump":
						map_bump = data[1];
					case "#":
						// コメント
					default:
						// unknown
						// throw "unknown data:" + data[0];
				}
			}
		}catch ( e:Eof ) {
			// おわり
			addGroup( );
		}

		return {
			materials: materials,
		};
	}

}
