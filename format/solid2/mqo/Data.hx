package format.solid2.mqo;

import format.solid2.data.UV;
import format.solid2.data.Vector;
import format.solid2.data.Surface;

typedef MQO = {
	version:String,
	scene: MQOScene,
	includeXmls:Array<String>,
	materials: Array<MQOMaterial>,
	objects: Array<MQOObject>,
};

typedef MQOScene = {
	pos:Vector,
	lookAt:Vector,
	head:Float,
	pich:Float,
	bank:Float,
	ortho:Bool,
	zoom2:Float,
	amb:Vector,
	frontclip:Float,
	backclip:Float,
	dirLights:Array<MQOLight>
};

typedef MQOLight = {
	dir:Vector,
	color:Vector,
};

typedef MQOMaterial = {
	name:String,
	shader:Int,
	vcol:Bool,
	col:Vector,
	dbls:Bool,
	dif:Float,
	amb:Float,
	emi:Float,
	spc:Float,
	power:Float,
	reflect:Float,
	refract:Float,
	tex:String,
	aplane:String,
	bump:String,
};

typedef MQOObject = {
	name:String,

	uid:Int,
	depth:Int,
	folding:Bool,

	scale:Vector,
	rotation:Vector,
	translation:Vector,

	patch:Int,
	patchtri:Bool,
	patchsmoothtri:Int,
	patchmeshinterp:Int,
	patchuvinterp:Int,
	segment:Int,

	visible:Int,
	locking:Bool,
	shading:Int,
	facet:Float,
	color:Vector,
	colorType:Int,
	mirror:Int,
	mirrorAxis:Int,
	mirrorDis:Float,
	lathe:Int,
	latheAxis:Int,
	latheSeg:Int,
	vertex:Array<Vector>,
	face:Array<MQOFace>,
};

typedef MQOFace = {
	m:Int,
	v:Surface,
	uv:Array<UV>,
	col:Array<Int>,
	crs:Array<Bool>,
};
