package format.solid2.rok;

import format.solid2.data.Vector;

typedef ROK = {
	viewPort:ROKViewPort,
	points:Array<ROKPoint>,
	lines:Array<ROKLine>,
	faces:Array<ROKFace>,
	palette:ROKPalette,
	back:ROKBack,
	lights:ROKLight,
	viewOption:ROKViewOption,
}

// 本家の2D_3D.CPPにあるRmatobj
typedef ROKmatobj = {
	pp:Float, pq:Float, pr:Float, // rotate angle
	dx:Float, dy:Float, dz:Float, // move coordinate
	bx:Float, by:Float, // scale to screen 2d coord
	pers:Bool,	// flag if pers:true or ortho:false
}

enum ROKViewMode {
	normal;
	yz;
	zx;
	xy;
}

typedef ROKViewPort = {
	transform:ROKmatobj,
	viewMode:ROKViewMode,
	scale2D:{ x:Float, y:Float },
	translate2D:{ x:Float, y:Float },
}

typedef ROKPoint = {
	id:Int,
	groupId:Int,
	visible:Bool,
	dummy:Int,
	mirrorPointId:Int,
	position:Vector,
}

typedef ROKLine = {
	from:Int,
	to:Int,
	groupId:Int,
	visible:Bool,
	property:ROKLineProperty,
}

typedef ROKFace = {
	count:Int,
	color:Int,
	id:Int,
	lines:Array<Int>,
}

typedef ROKPalette = {
	count:Int,
	selected:Int,
	colors:Array<ROKColor>,
}

typedef ROKColor = {
	bright:Vector,
	dark:Vector,
}

typedef ROKBack = {
	count:Int,
	selected:Int,
	colors:Array<Vector>,
}

typedef ROKLight = {
	count:Int,
	selected:Int,
	positions:Array<Vector>,
}

typedef ROKViewOption = {
	visibleLine:Bool,
	visibleFace:Bool,
}

typedef ROKLineProperty = {
	type:Int,
	color:Int,
	thin:Int,
}
