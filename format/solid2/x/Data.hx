package format.solid2.x;

import format.solid2.data.Surface;
import format.solid2.data.UV;
import format.solid2.data.Vector;

typedef X = {
	// ヘッダ
	header:XHeader,

	//  データ
	frames:Array<XFrame>,
	animationSets:Array<XAnimationSet>,
	animTicksPerSecond:Int,
};

typedef XHeader = {
	major:Int,
	minor:Int,
	type:String,
	bit:Int,
};

typedef XFrame = {
	name:String,
	frameTransformMatrix:Array<Float>,
	children:Array<XFrame>,
	meshes:Array<XMesh>,
};

typedef XMesh = {
	name:String,
	vertex:Array<Vector>,
	surfaces:Array<Surface>,
	normals:XMeshNormals,
	texCoords:XMeshTextureCoords,
	meshMaterialList:XMeshMaterialList,
	skinWeights:Array<XSkinWeight>,
};

typedef XMeshNormals = {
	normal:Array<Vector>,
	index:Array<Surface>,
};

typedef XMeshTextureCoords = {
	uvs:Array<UV>
};

typedef XMeshMaterialList = {
	index:Array<Int>,
	materials:Array<XMaterial>,
};

typedef XMaterial = {
	diffuse:Vector,
	power:Float,
	specular:Vector,
	emissive:Vector,
	tex:String,
};

typedef XSkinWeight = {
	target:String,
	index:Array<Int>,
	weight:Array<Float>,
	matrix:Array<Float>,
};

typedef XAnimationSet = {
	name:String,
	animations:Array<XAnimation>,
};

typedef XAnimation = {
	name:String,
	target:String,
	keys:Array<XAnimationKey>,
};

typedef XAnimationKey = {
	type:XAnimationKeyType,
	data:Array<XAnimationKeyData>,
};

enum XAnimationKeyType {
	scale;
	position;
	rotation;
	matrix;
}

typedef XAnimationKeyData = {
	frame:Int,
	data:Array<Float>,
};
