package format.solid2.x;

import format.solid2.data.Vector;
import format.solid2.data.UV;
import format.solid2.data.Surface;

enum Token
{
	TEof;

	TIden( s:String );
	TBegin;
	TEnd;
	TPOpen;
	TPClose;
	TBOpen;
	TBClose;
	TAOpen;
	TAClose;
	TComma;
	TColon;
	TSemicolon;
	CFloat( f:Float );
	CInt( i:Int );
	CString( s:String );
}

/**
 * X用レキサー
 * @author あるる（きのもと 結衣）
 */
class Lexer
{
	public var src( default, null ):String;
	public var len( default, null ):Int;
	public var line( default, null ):Int;
	public var p:Int;

	/**
	 * コンストラクタ
	 * @param src ソースコード
	 */
	public function new( src:String ) 
	{
		this.src = src;
		this.len = this.src.length;
		this.p = 0;
		this.line = 1;
	}

	/**
	 * error
	 * @param	msg
	 */
	private function error( msg:String ):Void
	{
		throw this.line + ":" + msg;
	}

	/**
	 * トークンがあるか？
	 * @return あるか否か
	 */
	public function hasNext( ):Bool
	{
		this.skipSpace( );
		return ( this.p < this.len );
	}

	/**
	 * コメントとスペースを読みとばす
	 */
	public function skipSpace( ):Void
	{
		var lineComment = false;

		while ( this.p < this.len ) {
			var through = false;
			var c = this.src.charAt( this.p );
			switch( c ) {
				case " ", "\t":
					through = true;
				case "\r":
					lineComment = false;
					through = true;
				case "\n":
					this.line ++;
					lineComment = false;
					through = true;
				case "#":
					lineComment = true;
				case "/":
					if( this.src.charAt( this.p + 1 ) == "/" ) {
						lineComment = true;
					}
			}

			if ( ( !lineComment ) && ( ! through ) ) break;
			this.p ++;
		}
	}

	/**
	 * トークン取得
	 * @return トークン
	 */
	public function next( ):Token
	{
		this.skipSpace( );
		if ( this.len <= this.p ) return Token.TEof;

		var c:String = this.src.charAt( this.p );

		this.p ++;
		switch( c ) {
			case "{": return Token.TBegin;
			case "}": return Token.TEnd;
			case "(": return Token.TPOpen;
			case ")": return Token.TPClose;
			case "[": return Token.TBOpen;
			case "]": return Token.TBClose;
			case "<": return Token.TAOpen;
			case ">": return Token.TAClose;
			case ",": return Token.TComma;
			case ":": return Token.TColon;
			case ";": return Token.TSemicolon;
			case "\"": return this.getString( );
			case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-":
				return this.getNumber( );
		}

		return this.getIden( );
	}

	/**
	 * 数字取得
	 * @return 数字トークン
	 */
	private function getNumber( ):Token
	{
		var str:StringBuf = new StringBuf( );
		var fFloat = false;

		this.p --;
		while ( this.p < this.len ) {
			var c = this.src.charAt( this.p );
			if ( "-0123456789.xABCDEFabcdef".indexOf( c ) == -1 ) break;
			if ( c == "." ) fFloat = true;
			str.add( c );
			this.p ++;
		}

		try {
			return if ( fFloat ) {
				Token.CFloat( Std.parseFloat( str.toString( ) ) );
			}else {
				Token.CInt( Std.parseInt( str.toString( ) ) );
			};
		}catch ( d:Dynamic ) {
			throw "lexer error: invalid number";
		};
	}

	/**
	 * 文字列取得
	 * @return 数字トークン
	 */
	private function getString( ):Token
	{
		var str:StringBuf = new StringBuf( );

		while ( this.p < this.len ) {
			var c = this.src.charAt( this.p );
			if ( "\"" == c ) break;
			str.add( c );
			this.p ++;
		}
		this.p ++;

		return Token.CString( str.toString( ) );
	}

	/**
	 * 識別子の取得
	 * @return 識別子トークンの取得
	 */
	private function getIden( ):Token
	{
		var str:StringBuf = new StringBuf( );
		str.add( this.src.charAt( this.p - 1 ) );

		// 識別子になりうる文字一覧
		var table:String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-@";

		while ( this.p < this.len ) {
			var c = this.src.charAt( this.p );
			if ( table.indexOf( c ) == -1 ) break;
			str.add( c );
			this.p ++;
		}

		return Token.TIden( str.toString( ) );
	}

	/**
	 * 必要なトークン
	 * @param	t
	 */
	public function expected( e:Token ):Void
	{
		var t = this.next( );
		if ( ! Type.enumEq( t, e ) ) {
			throw "expected :" + Std.string( e ) + " not " + Std.string( t );
		}
	}

	/**
	 * 1つのトークンを浮動小数点として読み込む
	 * @return
	 */
	public function readInteger( ):Int
	{
		var t = this.next( );

		return switch( t ) {
			case Token.CInt( i ): i;
			default:
				throw "invalid integer: " + t;
		};
	}

	/**
	 * 1つのトークンを浮動小数点として読み込む
	 * @return
	 */
	public function readFloat( ):Float
	{
		var t = this.next( );

		return switch( t ) {
			case Token.CFloat( f ): f;
			case Token.CInt( i ): i;
			default:
				throw "invalid number: " + t;
		};
	}

	/**
	 * 1つのトークンを文字列として読み込む
	 * @return
	 */
	public function readString( ):String
	{
		var t = this.next( );

		return switch( t ) {
			case Token.CString( s ): s;
			default:
				throw "invalid string: " + t;
		};
	}

	/**
	 * 1つのトークンを識別子として読み込む
	 * @return
	 */
	public function readIden( ):String
	{
		var t = this.next( );

		return switch( t ) {
			case Token.TIden( s ): s;
			default:
				throw "invalid iden:" + t;
		};
	}
	
	/**
	 * UV取得
	 * @return Vector
	 */
	public function readUV( ):UV
	{
		var u = this.readFloat( );
		this.expected( Token.TSemicolon );
		var v = this.readFloat( );
		this.expected( Token.TSemicolon );

		return {u:u,v:v};
	}
	
	/**
	 * 3点のVector取得
	 * @return Vector
	 */
	public function readVector3( ):Vector
	{
		var x = this.readFloat( );
		this.expected( Token.TSemicolon );
		var y = this.readFloat( );
		this.expected( Token.TSemicolon );
		var z = this.readFloat( );
		this.expected( Token.TSemicolon );

		return new Vector( x, y, z );
	}
	
	/**
	 * 4点のVector取得
	 * @return Vector
	 */
	public function readVector4( ):Vector
	{
		var x = this.readFloat( );
		this.expected( Token.TSemicolon );
		var y = this.readFloat( );
		this.expected( Token.TSemicolon );
		var z = this.readFloat( );
		this.expected( Token.TSemicolon );
		var w = this.readFloat( );
		this.expected( Token.TSemicolon );

		return new Vector( x, y, z, w );
	}

	/**
	 * 面データ読み込み
	 */
	public function readSurface( ):Surface
	{
		var counter = this.readInteger( );
		this.expected( Token.TSemicolon );

		return switch( counter ) {
			case 3:
				var a = this.readInteger( );
				this.expected( Token.TComma );
				var b = this.readInteger( );
				this.expected( Token.TComma );
				var c = this.readInteger( );
				this.expected( Token.TSemicolon );
				Surface.triangle( a, b, c );
			case 4:
				var a = this.readInteger( );
				this.expected( Token.TComma );
				var b = this.readInteger( );
				this.expected( Token.TComma );
				var c = this.readInteger( );
				this.expected( Token.TComma );
				var d = this.readInteger( );
				this.expected( Token.TSemicolon );
				Surface.quad( a, b, c, d );
			default:
				throw "invalid vertex count on surface";
		};
	}


	/**
	 * Int配列を読み込む
	 */
	public function readIntArray( ):Array<Int>
	{
		var a = new Array<Int>( );

		while ( true ) {
			var t = this.next( );
			switch( t ) {
				case Token.CInt( i ):
					a.push( i );
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}

			var t = this.next( );
			switch( t ) {
				case Token.TComma:
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return a;
	}

	/**
	 * Float配列を読み込む
	 */
	public function readFloatArray( ):Array<Float>
	{
		var a = new Array<Float>( );

		while ( true ) {
			var t = this.next( );
			switch( t ) {
				case Token.CFloat( f ):
					a.push( f );
				case Token.CInt( i ):
					a.push( i );
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}

			var t = this.next( );
			switch( t ) {
				case Token.TComma:
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return a;
	}

	/**
	 * Vertex配列を読み込む
	 */
	public function readVertexArray( ):Array<Vector>
	{
		var a = new Array<Vector>( );

		while ( true ) {
			a.push( this.readVector3( ) );

			var t = this.next( );
			switch( t ) {
				case Token.TComma:
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return a;
	}

	/**
	 * Surface配列を読み込む
	 */
	public function readSurfaceArray( ):Array<Surface>
	{
		var a = new Array<Surface>( );

		while ( true ) {
			a.push( this.readSurface( ) );

			var t = this.next( );
			switch( t ) {
				case Token.TComma:
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return a;
	}

	/**
	 * UV配列を返す
	 * @return
	 */
	public function readUVArray( ):Array<UV>
	{
		var a = new Array<UV>( );

		while ( true ) {
			a.push( this.readUV( ) );

			var t = this.next( );
			switch( t ) {
				case Token.TComma:
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return a;
	}
}
