package format.solid2.glb;

import format.solid2.glb.Data;
import haxe.io.Input;

/**
 * .glb reader
 */
class Reader
{
	private var bi:Input;

	/**
	 * コンストラクタ
	 * @param	i
	 */
	public function new( bi:Input ) 
	{
		this.bi = bi;
	}

	/**
	 * 読み込む
	 * @return
	 */
	public function read( ):GLB
	{
		if( this.bi.readString( 4 ) != "glTF" ) {
			throw "Unknown header.";
		}

		var version = this.bi.readInt32( );
		var length = this.bi.readInt32( );
		var p = 12;
		var chunks = new Array<GLBChunk>( );
		while( p < length ) {
			var chunk = this.readChunk( );
			chunks.push( chunk );
			p += 8 + chunk.data.length;
		}

		return {
			header:{version:version},
			chunks:chunks
		};
	}

	private function readChunk( ):GLBChunk
	{
		var length = this.bi.readInt32( );
		var type = this.bi.readString( 4 );
		var data = this.bi.read( length );
		return {
			type: type,
			data: data,
		}
	}
}