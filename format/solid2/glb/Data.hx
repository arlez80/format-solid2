package format.solid2.glb;

typedef GLB = {
	header:{version:Int},
	chunks:Array<GLBChunk>,
}

typedef GLBChunk = {
	type:String,
	data:haxe.io.Bytes
}
