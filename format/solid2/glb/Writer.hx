package format.solid2.glb;

import format.solid2.glb.Data;
import haxe.io.Output;

/**
 * .glb Writer
 * @author あるる（きのもと 結衣）
 */
class Writer 
{
	private var bo:Output;

	/**
	 * コンストラクタ
	 * @param	bo
	 */
	public function new( bo:Output ) 
	{
		this.bo = bo;
	}

	/**
	 * 書き込む
	 * @param	mqo
	 */
	public function write( glb:GLB ):Void
	{
		var size = 12;
		for( chunk in glb.chunks ) {
			size += 8 + chunk.data.length;
		}

		this.bo.writeString( "glTF" );
		this.bo.writeInt32( 2 );
		this.bo.writeInt32( size );

		for( chunk in glb.chunks ) {
			this.bo.writeInt32( chunk.data.length );
			for( i in 0 ... 4 ) {
				var c = if( i < chunk.type.length ) chunk.type.charCodeAt( i ) else 0;
				this.bo.writeByte( c );
			}
			this.bo.write( chunk.data );
		}
	}
}
