package format.solid2.data;

class Vector
{
	public var x:Float;
	public var y:Float;
	public var z:Float;
	public var w:Float;

	public function new( x:Float = 0.0, y:Float = 0.0, z:Float = 0.0, w:Float = 0.0 )
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public var r(get,set):Float;
	public var g(get,set):Float;
	public var b(get,set):Float;
	public var a(get,set):Float;

	public function set_r( v:Float ):Float
	{
		return this.x = v;
	}
	public function get_r( ):Float
	{
		return this.x;
	}

	public function set_g( v:Float ):Float
	{
		return this.y = v;
	}
	public function get_g( ):Float
	{
		return this.y;
	}

	public function set_b( v:Float ):Float
	{
		return this.z = v;
	}
	public function get_b( ):Float
	{
		return this.z;
	}

	public function set_a( v:Float ):Float
	{
		return this.w = v;
	}
	public function get_a( ):Float
	{
		return this.w;
	}

	public function toArray3( ):Array<Float>
	{
		return [this.x, this.y, this.z ];
	}

	public function toArray4( ):Array<Float>
	{
		return [this.x, this.y, this.z, this.w ];
	}
}
