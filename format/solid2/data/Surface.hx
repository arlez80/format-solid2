package format.solid2.data;

enum Surface {
	triangle( a:Int, b:Int, c:Int );
	quad( a:Int, b:Int, c:Int, d:Int );
}
