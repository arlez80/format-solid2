package format.solid2.gltf;

/**
 * for GLTF 2.0
 */
typedef GLTF = {
	?extensionsUsed:Array<String>,
	?extensionsRequired:Array<String>,

	?accessors:Array<GLTFAccessor>,
	?animations:Array<GLTFAnimation>,
	asset:GLTFAsset,
	?buffers:Array<GLTFBuffer>,
	?bufferViews:Array<GLTFBufferView>,
	?cameras:Array<GLTFCamera>,
	?images:Array<GLTFImage>,
	?materials:Array<GLTFMaterial>,
	?meshes:Array<GLTFMesh>,
	?nodes:Array<GLTFNode>,
	?samplers:Array<GLTFSampler>,
	?scene:Int,
	?scenes:Array<GLTFScene>,
	?skins:Array<GLTFSkin>,
	?textures:Array<GLTFTexture>,
}

@:enum abstract GLTFCommponentType(Int) from Int to Int {
	var BYTE = 5120;
	var UNSIGNED_BYTE = 5121;
	var SHORT = 5122;
	var UNSIGNED_SHORT = 5123;
	var INT = 5124;
	var UNSIGNED_INT = 5125;
	var FLOAT = 5126;
}

@:enum abstract GLTFAccessorType(String) from String to String {
	var SCALAR = "SCALAR";
	var VEC2 = "VEC2";
	var VEC3 = "VEC3";
	var VEC4 = "VEC4";
	var MAT2 = "MAT2";
	var MAT3 = "MAT3";
	var MAT4 = "MAT4";
}

@:enum abstract GLTFInterpolation(String) from String to String {
	var STEP = "STEP";
	var LINEAR = "LINEAR";
	var CUBICSPLINE = "CUBICSPLINE";
}

@:enum abstract GLTFAnimationChannelTargetPath(String) from String to String {
	var ROTATION = "rotation";
	var TRANSLATION = "translation";
	var SCALE = "scale";
	var weights = "weights";
}

@:enum abstract GLTFCameraType(String) from String to String {
	var perspective = "perspective";
	var orthographic = "orthographic";
}

@:enum abstract GLTFBufferViewTarget(Int) from Int to Int {
	var ARRAY_BUFFER = 34962;
	var ELEMENT_ARRAY_BUFFER = 34963;
}

@:enum abstract GLTFMaterialAlphaMode(String) from String to String {
	var OPAQUE = "OPAQUE";
	var MASK = "MASK";
	var BLEND = "BLEND";
}

@:enum abstract GLTFMeshPrimitive(Int) from Int to Int {
	var POINTS = 0;
	var LINES = 1;
	var LINE_LOOP = 2;
	var LINE_STRIP = 3;
	var TRIANGLES = 4;
	var TRIANGLE_STRIP = 5;
	var TRIANGLE_FAN = 6;
}

@:enum abstract GLTFSamplerFilter(Int) from Int to Into {
	var NEAREST = 9728;
	var LINEAR = 9729;

	var NEAREST_MIPMAP_NEAREST = 9984;
	var LINEAR_MIPMAP_NEAREST = 9985;
	var NEAREST_MIPMAP_LINEAR = 9986;
	var LINEAR_MIPMAP_LINEAR = 9987;
}

@:enum abstract GLTFSamplerWrap(Int) from Int to Int {
	var CLAMP_TO_EDGE = 33071;
	var MIRRORED_REPEAT = 33648;
	var REPEAT = 10497;
}

typedef GLTFAccessor = {
	name:String,

	?bufferView:Int,
	?byteOffset:Int,
	componentType:GLTFCommponentType,
	?normalized:Bool,
	count:Int,
	type:GLTFAccessorType,
	?max:Array<Float>,
	?min:Array<Float>,
	?sparse:GLTFAccessorSparse,
}

typedef GLTFAccessorSparse = {
	count:Int,
	indices:Array<GLTFAccessorSparseIndex>,
	values:Array<GLTFAccessorSparseValue>,
}

typedef GLTFAccessorSparseIndex = {
	bufferView:Int,
	?byteOffset:Int,
	componentType:GLTFCommponentType,
}

typedef GLTFAccessorSparseValue = {
	bufferView:Int,
	?byteOffset:Int,
}

typedef GLTFAnimation = {
	name:String,

	samplers:Array<GLTFAnimationSampler>,
	channels:Array<GLTFAnimationChannel>,
}

typedef GLTFAnimationSampler = {
	input:Int,
	?interpolation:GLTFInterpolation,
	output:Int,
}

typedef GLTFAnimationChannel = {
	sampler:Int,
	target:GLTFAnimationChannelTarget,
}

typedef GLTFAnimationChannelTarget = {
	?node:Int,
	path:GLTFAnimationChannelTargetPath,
}

typedef GLTFAsset = {
	?copyright:String,
	?generator:String,
	version:String,
	?minVersion:String,
}

typedef GLTFBuffer = {
	name:String,

	uri:String,
	byteLength:Int,

	// added by Reader
	data:haxe.io.Bytes,
	path:String,
	embed:Bool,
}

typedef GLTFBufferView = {
	buffer:Int,
	?byteOffset:Int,
	byteLength:Int,
	?byteStride:Int,
	?target:GLTFBufferViewTarget,
}

typedef GLTFCamera = {
	name:String,

	?orthographic:GLTFCameraOrthographic,
	?perspective:GLTFCameraPerspective,
	type:GLTFCameraType,
}

typedef GLTFCameraOrthographic = {
	?aspectRatio:Float,
	yfov:Float,
	?zfar:Float,
	znear:Float,
}

typedef GLTFCameraPerspective = {
	xmag:Float,
	ymag:Float,
	zfar:Float,
	znear:Float,
}

typedef GLTFImage = {
	name:String,

	?uri:String,
	?bufferView:Int,
	?mimeType:String,
}

typedef GLTFMaterial = {
	name:String,

	// check are these optional fields?
	pbrMetallicRoughness:GLTFPbrMetallicRoughness,
	normalTexture:GLTFMaterialNormalTextureInfo,
	occlusionTexture:GLTFMaterialOcclusionTextureInfo,
	emissiveTexture:GLTFTextureInfo,
	emissiveFactor:Array<Float>,
	alphaMode:GLTFMaterialAlphaMode,
	alphaCutoff:Float,
	doubleSided:Bool,
}

typedef GLTFPbrMetallicRoughness = {
	baseColorFactor:Array<Float>,
	baseColorTexture:GLTFTextureInfo,
	metallicFactor:Float,
	roughnessFactor:Float,
	metallicRoughnessTexture:GLTFTextureInfo,
}

typedef GLTFTextureInfo = {
	index:Int,
	?texCoord:Int,
}

typedef GLTFMaterialNormalTextureInfo = {
	index:Int,
	?texCoord:Int,
	strength:Float,
}

typedef GLTFMaterialOcclusionTextureInfo = {
	index:Int,
	?texCoord:Int,
	strength:Float,
}

typedef GLTFMesh = {
	name:String,

	primitives:Array<GLTFMeshPrimitive>,
	?weights:Array<Float>,
}

typedef GLTFMeshPrimitive = {
	attributes:GLTFMeshPrimitiveAttributes,
	?indices:Int,
	?material:Int,
	?mode:GLTFMeshPrimitiveMode,
	?targets:Array<Int>
}

typedef GLTFMeshPrimitiveAttributes = {
	?POSITION:Int,
	?NORMAL:Int,
	?TANGENT:Int,
	?TEXCOORD_0:Int,
	?TEXCOORD_1:Int,
	?COLOR_0:Int,
	?JOINTS_0:Int,
	?WEIGHTS_0:Int,
}

typedef GLTFNode = {
	name:String,

	?camera:Int,
	?children:Array<Int>,
	?skin:Int,
	matrix:Array<Float>,
	?mesh:Int,
	?rotation:Array<Float>,
	?scale:Array<Float>,
	?translation:Array<Float>,
	?weights:Float,
}

typedef GLTFSampler = {
	name:String,

	magFilter:GLTFSamplerFilter,
	minFilter:GLTFSamplerFilter,
	wrapS:GLTFSamplerWrap,
	wrapT:GLTFSamplerWrap,
}

typedef GLTFScene = {
	name:String,

	nodes:Array<Int>,
}

typedef GLTFSkin = {
	name:String,

	?inverseBindMatrices:Int,
	?skeleton:Int,
	joints:Array<Int>,
}

typedef GLTFTexture = {
	name:String,

	sampler:Int,
	source:Int,
}
