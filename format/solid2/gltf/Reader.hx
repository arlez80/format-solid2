package format.solid2.gltf;

import haxe.crypto.Base64;
import haxe.Json;
import haxe.io.Input;
import format.solid2.gltf.Data;
import format.solid2.data.Vector;

/**
 * .gltf Reader
 * @author あるる（きのもと 結衣）
 */
class Reader
{
	private var src:Dynamic;
	private var files:Map<String,haxe.io.Bytes>;
	public var loadedBinary:haxe.io.Bytes;

	private var buffers:Array<GLTFBuffer>;

	/**
	 * コンストラクタ
	 */
	public function new( bi:Input )
	{
		this.src = Json.parse( bi.readAll( ).toString( ) );
		this.files = new Map<String,haxe.io.Bytes>( );
	}

	/**
	 * ファイル追加
	 * @param name 
	 * @param data 
	 */
	public function addFile( name:String, data:haxe.io.Bytes ):Void
	{
		this.files.set( name, data );
	}

	/**
	 * 読み込み
	 * @return GLTF
	 */
	public function read( ):GLTF
	{
		this.readBuffers( );
		// this.readMeshes( );

		return {
			asset: this.src.asset,
			scene: this.src.scene,
			scenes: this.src.scenes,
			nodes: this.src.nodes,
			meshes: this.src.meshes,
			animations: this.src.animations,
			skins: this.src.skins,
			accessors: this.src.accessors,
			materials: this.src.materials,
			textures: this.src.textures,
			images: this.src.images,
			samplers: this.src.samplers,
			bufferViews: this.src.bufferViews,
			buffers: this.buffers,
		};
	}

	/**
	 * メッシュ
	 */
	private function readMeshes( ):Void
	{
		var src:Array<{primitives:Array<{attributes:Dynamic,indices:Int,mode:Int,material:Int}>,name:String}> = this.src.meshes;
		this.meshes = new Array<GLTFMesh>( );

		for( t in src ) {
			var primitives = new Array<GLTFMeshPrimitive>( );
			for( primitive in t.primitives ) {
				var attributes:GLTFMeshPrimitiveAttribute = {
					joints:[],
					normal:null,
					position:null,
					texcoords:[],
					weights:[],
				};
				for( key in Reflect.fields( primitive.attributes ) ) {
					var value:Int = Reflect.field( primitive.attributes, key );
					var type = key.split( "_" )[0];
					var index = Std.parseInt( key.split( "_" )[1] );
					switch( type ) {
						case "JOINTS": attributes.joints[index] = value;
						case "NORMAL": attributes.normal = value;
						case "POSITION": attributes.position = value;
						case "TEXCOORD": attributes.texcoords[index] = value;
						case "WEIGHTS": attributes.weights[index] = value;
						default:
							// throw "unknown primitive-attribute"
					}
				}

				primitives.push({
					attributes: attributes,
					indices: primitive.indices,
					mode: primitive.mode,
					material: primitive.material,
				});
			}

			this.meshes.push({
				name: t.name,
				primitives: primitives,
			});
		}
	}

	/**
	 * バッファ読み込み
	 */
	private function readBuffers( ):Void
	{
		var src:Array<{byteLength:Int,?uri:String}> = this.src.buffers;

		this.buffers = new Array<GLTFBuffer>( );
		for( t in src ) {
			if( t.uri == null ) {
				this.buffers.push({
					data: this.loadedBinary,
					byteLength: t.byteLength,
					embed: false,
					path: '',
					uri: null,
				});
			}else {
				this.buffers.push( this.readUri( t.uri ) );
			}
		}
	}

	/**
	 * パスからバイナリを読み込む
	 * @param	uri
	 * @return GLTFBuffer
	 */
	private function readUri( uri:String ):GLTFBuffer
	{
		var base64 = "data:application/octet-stream;base64";
		var embed = false;
		var path = "";

		var data = if( uri.substr( 0, base64.length) == base64 ) {
			embed = true;
			Base64.decode( uri.substr( base64.length + 1 ) );
		}else if( this.files.exists( uri ) ) {
			path = uri;
			this.files.get( uri );
		}else {
			path = uri;
			sys.io.File.getBytes( uri );
		};

		return {
			data: data,
			byteLength: data.length,
			embed: embed,
			path: path,
			uri: uri,
		};
	}
}
