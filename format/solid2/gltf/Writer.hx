package format.solid2.gltf;

import haxe.crypto.Base64;
import haxe.io.Output;
import haxe.Json;
import format.solid2.gltf.Data;

using Lambda;

/**
 * .gltf Writer
 * @author あるる（きのもと 結衣）
 * 
 * TODO
 */
class Writer
{
	private var gltf:GLTF;
	private var bo:Output;

	/**
	 * コンストラクタ
	 * @param	bo
	 */
	public function new( bo:Output ) 
	{
		this.bo = bo;
	}

	/**
	 * 書き込む
	 * @param	gltf
	 */
	public function write( gltf:GLTF ):Void
	{
		this.gltf = gltf;

		this.bo.writeString(Json.stringify({
			asset: gltf.asset,
			scene: gltf.scene,
			scenes: gltf.scenes,
			nodes: gltf.nodes,
			meshes: this.writeMeshes( ),
			animations: gltf.animations,
			skins: gltf.skins,
			accessors: gltf.accessors,
			materials: gltf.materials,
			textures: gltf.textures,
			images: gltf.images,
			samplers: gltf.samplers,
			bufferViews: gltf.bufferViews,
			buffers: this.writeBuffers( ),
		}));
	}

	/**
	 * メッシュ書き込み
	 */
	private function writeMeshes( ):Array<{primitives:Array<{attributes:Dynamic,indices:Int,mode:Int,material:Int}>,name:String}>
	{
		var dest = new Array<{primitives:Array<{attributes:Dynamic,indices:Int,mode:Int,material:Int}>,name:String}>( );

		for( t in this.gltf.meshes ) {
			var primitives = new Array<{attributes:Dynamic,indices:Int,mode:Int,material:Int}>( );
			for( primitive in t.primitives ) {
				var attributes = {};
				// joints
				var id:Int = 0;
				for( t in primitive.attributes.joints ) {
					Reflect.setField( attributes, "JOINTS_" + Std.string(id), t );
					id ++;
				}
				// normal
				Reflect.setField( attributes, "NORMAL_0", primitive.attributes.normal );
				// position
				Reflect.setField( attributes, "POSITION_0", primitive.attributes.position );
				// texcoords
				var id:Int = 0;
				for( t in primitive.attributes.texcoords ) {
					Reflect.setField( attributes, "TEXCOORD_" + Std.string(id), t );
					id ++;
				}
				// weights
				var id:Int = 0;
				for( t in primitive.attributes.weights ) {
					Reflect.setField( attributes, "WEIGHTS_" + Std.string(id), t );
					id ++;
				}

				primitives.push({
					attributes: attributes,
					indices: primitive.indices,
					mode: primitive.mode,
					material: primitive.material,
				});
			}

			dest.push({
				name: t.name,
				primitives: primitives,
			});
		}

		return dest;
	}

	private function writeBuffers( ):Array<{?uri:String, byteLength:Int}>
	{
		var a = new Array<{?uri:String, byteLength:Int}>( );

		for( t in this.gltf.buffers ) {
			if( t.embed ) {
				a.push({
					uri: "data:application/octet-stream;base64" + Base64.encode( t.data ),
					byteLength: t.data.length,
				});
			}else {
				a.push({
					uri: t.uri,
					byteLength: t.byteLength,
				});
			}
		}

		return a;
	}
}
