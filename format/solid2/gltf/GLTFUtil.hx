package format.solid2.gltf;

import haxe.io.BytesInput;
import format.solid2.gltf.Data;
import format.solid2.data.Vector;

/**
 * GLTF Utility
 */
class GLTFUtil
{
	/**
	 * スカラー値を取得
	 * @return Array<Float>
	 */
	static public function getScalar( accessor:GLTFAccessor ):Array<Float>
	{
		if( accessor.type != SCALAR ) {
			throw "try to convert non scalar array to scalar array.";
		}

		var bufferView = accessor.bufferView;
		var bvData = bufferView.buffer.data.sub( bufferView.offset, bufferView.length );
		var bi = new BytesInput( bvData, accessor.offset );

		var v = new Array<Float>( );
		for( i in 0 ... accessor.count ) {
			v.push( bi.readFloat( ) );
		}

		return v;
	}

	/**
	 * Vector 3値を取得
	 * @return Array<Vector>
	 */
	static public function getVec3( accessor:GLTFAccessor ):Array<Vector>
	{
		if( accessor.type != VEC3 ) {
			throw "try to convert non vec3 array to vec3 array.";
		}

		var bufferView = accessor.bufferView;
		var bvData = bufferView.buffer.data.sub( bufferView.offset, bufferView.length );
		var bi = new BytesInput( bvData, accessor.offset );

		var v = new Array<Vector>( );
		for( i in 0 ... accessor.count ) {
			var x = bi.readFloat( );
			var y = bi.readFloat( );
			var z = bi.readFloat( );
			v.push( new Vector( x, y, z ) );
		}

		return v;
	}

	/**
	 * Vector 4値を取得
	 * @return Array<Vector>
	 */
	static public function getVec4( accessor:GLTFAccessor ):Array<Vector>
	{
		if( accessor.type != VEC4 ) {
			throw "try to convert non vec4 array to vec4 array.";
		}

		var bufferView = accessor.bufferView;
		var bvData = bufferView.buffer.data.sub( bufferView.offset, bufferView.length );
		var bi = new BytesInput( bvData, accessor.offset );

		var v = new Array<Vector>( );
		for( i in 0 ... accessor.count ) {
			var x = bi.readFloat( );
			var y = bi.readFloat( );
			var z = bi.readFloat( );
			var w = bi.readFloat( );
			v.push( new Vector( x, y, z, w ) );
		}

		return v;
	}
	/**
	 * 配列 4x4値を取得
	 * @return Array<Array<Float>>
	 */
	static public function getMat4( accessor:GLTFAccessor ):Array<Array<Float>>
	{
		if( accessor.type != MAT4 ) {
			throw "try to convert non mat4 array to mat4 array.";
		}

		var bufferView = accessor.bufferView;
		var bvData = bufferView.buffer.data.sub( bufferView.offset, bufferView.length );
		var bi = new BytesInput( bvData, accessor.offset );

		var v = new Array<Array<Float>>( );
		for( i in 0 ... accessor.count ) {
			v.push( [for( k in 0 ... 16 ) bi.readFloat( )] );
		}

		return v;
	}
}
