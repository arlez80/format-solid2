package format.solid2.tjs;

import format.solid2.data.Vector;
import format.solid2.data.UV;

typedef ThreeJS4Model = {
	metadata:Dynamic,

	geometries:Array<ThreeJS4Geometry>,
	materials:Array<ThreeJS4Material>,
	object:ThreeJS4Object,
}

typedef ThreeJS4Geometry = {
	uuid:String,
	type:String,
	data:ThreeJS4GeometryData,
}

typedef ThreeJS4GeometryData = {
	positions:Array<Vector>,
	normals:Array<Vector>,
	uvs:Array<UV>,
	boundingSphere:{center:Vector, radius:Float},
}

typedef ThreeJS4Material = {
	uuid:String,
	type:String,
	color:Int,
	ambient:Int,
	emissive:Int,
	specular:Int,
	shininess:Int,
	opacity:Float,
	transparent:Bool,
	wireframe:Bool,
}

typedef ThreeJS4Object = {
	uuid:String,
	type:String,
	matrix:Array<Float>,
	geometry:ThreeJS4Geometry,
	material:ThreeJS4Material,
	children:Array<ThreeJS4Object>,
}
