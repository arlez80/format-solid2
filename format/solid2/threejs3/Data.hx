package format.solid2.threejs3;

import format.solid2.data.Surface;
import format.solid2.data.UV;
import format.solid2.data.Vector;

class ThreeJS3ModelFaceType
{
	// inline static public var TRIANGLE = 0;
	inline static public var QUAD = 1;
	inline static public var FACE_MATERIAL = 2;
	inline static public var FACE_UV = 4;
	inline static public var FACE_VERTEX_UV = 8;
	inline static public var FACE_NORMAL = 16;
	inline static public var FACE_VERTEX_NORMAL = 32;
	inline static public var FACE_COLOR = 64;
	inline static public var FACE_VERTEX_COLOR = 128;
}

typedef ThreeJS3Model = {
	metadata:Dynamic,

	faces:Array<ThreeJSSurface>,
	materials:Array<ThreeJS3Material>,
	vertices:Array<Vector>,
	normals:Array<Vector>,
	colors:Array<Vector>,
	uvs:Array<Array<UV>>,
}

typedef ThreeJSSurface = {
	vertexIndex:Surface,
	materialIndex:Null<Int>,
	faceUV:Null<Int>,
	faceVertexUV:Surface,
	faceNormal:Null<Int>,
	faceVertexNormal:Surface,
	faceColor:Null<Int>,
	faceVertexColor:Surface,
}

typedef ThreeJS3Material = {
	colorDiffuse:Vector,
	colorEmissive:Vector,
	colorSpecular:Vector,
	colorAmbient:Vector,

	depthWrite:Bool,
	depthTest:Bool,
	transparent:Bool,
	visible:Bool,
	wireframe:Bool,

	specularCoef:Float,
	opacity:Float,

	blending:String,
	shading:String,
}

// ------------------------------------------
// 以下は内部で使用する

typedef ThreeJS3MaterialRaw = {
	colorDiffuse:Array<Float>,
	colorEmissive:Array<Float>,
	colorSpecular:Array<Float>,
	colorAmbient:Array<Float>,

	depthWrite:Bool,
	depthTest:Bool,
	transparent:Bool,
	visible:Bool,
	wireframe:Bool,

	specularCoef:Float,
	opacity:Float,

	blending:String,
	shading:String,
}
