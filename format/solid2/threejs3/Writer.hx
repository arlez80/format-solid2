package format.solid2.threejs3;

import haxe.Json;
import haxe.io.Output;
import format.solid2.threejs3.Data;
import format.solid2.data.Vector;
import format.solid2.data.UV;

/**
 * three.js model 3 Writer
 * @author あるる（きのもと 結衣）
 */
class Writer
{
	private var bo:Output;

	/**
	 * コンストラクタ
	 * @param	bo
	 */
	public function new( bo:Output )
	{
		this.bo = bo;
	}

	/**
	 * 出力
	 * @param tjs 
	 */
	public function write( tjs:ThreeJS3Model ):Void
	{
		this.bo.writeString(Json.stringify({
			metadata: tjs.metadata,

			faces: this.convFaces( tjs.faces ),
			materials: this.convMaterials( tjs.materials ),
			vertices: this.convVectors( tjs.vertices ),
			normals: this.convVectors( tjs.normals ),
			colors: this.convVectors( tjs.colors ),
			uvs: this.convUVs( tjs.uvs ),
		}));
	}

	/**
	 * ベクトルを変換
	 * @param d 
	 * @return Array<Float>
	 */
	private function convVectors( d:Array<Vector> ):Array<Float>
	{
		var v = new Array<Float>( );
		for( t in d ) {
			v.push( t.x );
			v.push( t.y );
			v.push( t.z );
		}
		return v;
	}

	/**
	 * UVを変換
	 * @param d 
	 * @return Array<Float>
	 */
	private function convUVs( d:Array<Array<UV>> ):Array<Array<Float>>
	{
		var v = new Array<Array<Float>>( );
		for( t in d ) {
			var w = new Array<Float>( );
			for( c in t ) {
				w.push( c.u );
				w.push( c.v );
			}
			v.push( w );
		}
		return v;
	}

	/**
	 * マテリアルを変換
	 * @param d 
	 * @return Array<Float>
	 */
	private function convMaterials( d:Array<ThreeJS3Material> ):Array<ThreeJS3MaterialRaw>
	{
		var v = new Array<ThreeJS3MaterialRaw>( );

		function convVector( a:Vector ):Array<Float> {
			return [ a.x, a.y, a.z ];
		}

		for( t in d ) {
			v.push({
				colorDiffuse: convVector( t.colorDiffuse ),
				colorEmissive: convVector( t.colorEmissive ),
				colorSpecular: convVector( t.colorSpecular ),
				colorAmbient: convVector( t.colorAmbient ),

				depthWrite: t.depthWrite,
				depthTest: t.depthTest,
				transparent: t.transparent,
				visible: t.visible,
				wireframe: t.wireframe,

				specularCoef: t.specularCoef,
				opacity: t.opacity,

				blending: t.blending,
				shading: t.shading,
			});
		}

		return v;
	}

	/**
	 * 面データ変換
	 * @return Array<Int>
	 */
	private function convFaces( d:Array<ThreeJSSurface> ):Array<Int>
	{
		var v = new Array<Int>( );

		for( t in d ) {
			var type:Int = 0;
			var face = new Array<Int>( );
			switch( t.vertexIndex ) {
				case triangle(a,b,c):
					face.push(a); face.push(b); face.push(c);
				case quad(a,b,c,d):
					face.push(a); face.push(b); face.push(c); face.push(d);
					type |= ThreeJS3ModelFaceType.QUAD;
			}
			if( t.materialIndex != null ) {
				face.push( t.materialIndex );
				type |= ThreeJS3ModelFaceType.FACE_MATERIAL;
			}
			if( t.faceUV != null ) {
				face.push( t.faceUV );
				type |= ThreeJS3ModelFaceType.FACE_UV;
			}
			if( t.faceVertexUV != null ) {
				switch( t.faceVertexUV ) {
					case triangle(a,b,c):
						face.push(a); face.push(b); face.push(c);
					case quad(a,b,c,d):
						face.push(a); face.push(b); face.push(c); face.push(d);
				}
				type |= ThreeJS3ModelFaceType.FACE_VERTEX_UV;
			}
			if( t.faceNormal != null ) {
				face.push( t.faceNormal );
				type |= ThreeJS3ModelFaceType.FACE_NORMAL;
			}
			if( t.faceVertexNormal != null ) {
				switch( t.faceVertexNormal ) {
					case triangle(a,b,c):
						face.push(a); face.push(b); face.push(c);
					case quad(a,b,c,d):
						face.push(a); face.push(b); face.push(c); face.push(d);
				}
				type |= ThreeJS3ModelFaceType.FACE_VERTEX_NORMAL;
			}
			if( t.faceColor != null ) {
				face.push( t.faceColor );
				type |= ThreeJS3ModelFaceType.FACE_COLOR;
			}
			if( t.faceVertexColor != null ) {
				switch( t.faceVertexColor ) {
					case triangle(a,b,c):
						face.push(a); face.push(b); face.push(c);
					case quad(a,b,c,d):
						face.push(a); face.push(b); face.push(c); face.push(d);
				}
				type |= ThreeJS3ModelFaceType.FACE_VERTEX_COLOR;
			}

			v.push( type );
			v = v.concat( face );
		}

		return v;
	}
}
