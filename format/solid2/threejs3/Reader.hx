package format.solid2.threejs3;

import haxe.Json;
import haxe.io.Input;
import format.solid2.threejs3.Data;
import format.solid2.data.Vector;
import format.solid2.data.UV;
import format.solid2.data.Surface;

/**
 * three.js model 3 Reader
 * @author あるる（きのもと 結衣）
 */
class Reader
{
	private var src:Dynamic;

	/**
	 * コンストラクタ
	 */
	public function new( bi:Input )
	{
		this.src = Json.parse( bi.readAll( ).toString( ) );
	}

	/**
	 * 読み込む
	 * @return ThreeJS3Model
	 */
	public function read( ):ThreeJS3Model
	{
		return {
			metadata: this.src.metadata,

			faces: this.readFaces( ),
			materials: this.readMaterials( ),
			vertices: this.readVectors( this.src.vertices ),
			normals: this.readVectors( this.src.normals ),
			colors: this.readVectors( this.src.colors ),
			uvs: this.readUVs( ),
		};
	}

	/**
	 * ベクトルデータを読む
	 * @return Array<Vector>
	 */
	private function readVectors( d:Array<Float> ):Array<Vector>
	{
		var v = new Array<Vector>( );
		if( d == null ) return v;

		var i = 0;
		var max = d.length;
		while( i < max ) {
			v.push( new Vector( d[i+0], d[i+1], d[i+2] ) );
			i += 3;
		}

		return v;
	}

	/**
	 * UVを読む
	 * @return Array<Array<UV>>
	 */
	private function readUVs( ):Array<Array<UV>>
	{
		var v = new Array<Array<UV>>( );
		var d:Array<Array<Float>> = this.src.uvs;
		if( d == null ) return v;

		for( t in d ) {
			var w = new Array<UV>( );
			var i = 0;
			var max = t.length;
			while( i < max ) {
				w.push( {u:t[i+0], v:t[i+1]} );
				i += 2;
			}
			v.push(w);
		}

		return v;
	}

	/**
	 * マテリアルデータ
	 * @return Array<ThreeJS3Material>
	 */
	private function readMaterials( ):Array<ThreeJS3Material>
	{
		var v = new Array<ThreeJS3Material>( );
		var d:Array<ThreeJS3MaterialRaw> = this.src.materials;
		if( d == null ) return v;

		function convVector( a:Dynamic ):Vector {
			if( a == null ) return null;
			return new Vector( a[0], a[1], a[2] );
		}

		for( t in d ) {
			v.push({
				colorDiffuse: convVector( t.colorDiffuse ),
				colorEmissive: convVector( t.colorEmissive ),
				colorSpecular: convVector( t.colorSpecular ),
				colorAmbient: convVector( t.colorAmbient ),

				depthWrite: t.depthWrite,
				depthTest: t.depthTest,
				transparent: t.transparent,
				visible: t.visible,
				wireframe: t.wireframe,

				specularCoef: t.specularCoef,
				opacity: t.opacity,

				blending: t.blending,
				shading: t.shading,
			});
		}

		return v;
	}

	/**
	 * 面データ
	 * @return Array<ThreeJSSurface>
	 */
	private function readFaces( ):Array<ThreeJSSurface>
	{
		var v = new Array<ThreeJSSurface>( );
		var d:Array<Int> = this.src.faces;
		if( d == null ) return v;

		var i = 0;
		var max = d.length;

		while( i < max ) {
			var type = d[i];
			var points = ( ( type & ThreeJS3ModelFaceType.QUAD ) != 0 ) ? 4 : 3;

			function getSurface( ):Surface
			{
				var x = d[i ++];
				var y = d[i ++];
				var z = d[i ++];
				return switch( points ) {
					case 3:
						Surface.triangle( x, y, z );
					case 4:
						var w = d[i ++];
						Surface.quad( x, y, z, w );
					default:
						throw "unknown points";
				};
			}

			var vertexIndex = getSurface( );
			var materialIndex:Null<Int> = if( ( type & ThreeJS3ModelFaceType.FACE_MATERIAL ) != 0 ) {
				d[i ++];
			}else {
				null;
			};
			var faceUV:Null<Int> = if( ( type & ThreeJS3ModelFaceType.FACE_UV ) != 0 ) {
				d[i ++];
			}else {
				null;
			};
			var faceVertexUV = if( ( type & ThreeJS3ModelFaceType.FACE_VERTEX_UV ) != 0 ) {
				getSurface( );
			}else {
				null;
			};
			var faceNormal:Null<Int> = if( ( type & ThreeJS3ModelFaceType.FACE_NORMAL ) != 0 ) {
				d[i ++];
			}else {
				null;
			};
			var faceVertexNormal = if( ( type & ThreeJS3ModelFaceType.FACE_VERTEX_NORMAL ) != 0 ) {
				getSurface( );
			}else {
				null;
			};
			var faceColor:Null<Int> = if( ( type & ThreeJS3ModelFaceType.FACE_COLOR ) != 0 ) {
				d[i ++];
			}else {
				null;
			};
			var faceVertexColor = if( ( type & ThreeJS3ModelFaceType.FACE_VERTEX_COLOR ) != 0 ) {
				getSurface( );
			}else {
				null;
			};

			v.push({
				vertexIndex: vertexIndex,
				materialIndex: materialIndex,
				faceUV: faceUV,
				faceVertexUV: faceVertexUV,
				faceNormal: faceNormal,
				faceVertexNormal: faceVertexNormal,
				faceColor: faceColor,
				faceVertexColor: faceVertexColor,
			});
		}

		return v;
	}
}
