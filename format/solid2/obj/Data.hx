package format.solid2.obj;

import format.solid2.data.Vector;
import format.solid2.data.UV;
import format.solid2.data.Surface;

typedef OBJ = {
	mtllib:String,
	groups:Array<OBJGroup>,
};

typedef OBJFace = {
	index:Surface,
	uv:Surface,
};

typedef OBJGroup = {
	g:String,
	usemtl:String,
	v:Array<Vector>,
	vt:Array<UV>,
	vn:Array<Vector>,
	f:Array<OBJFace>,
};
