package ;

import haxe.zip.Reader;
import haxe.io.BytesInput;
import haxe.io.BytesOutput;
import sys.io.File;

class Main
{
	static public function main( )
	{
		new Main( );
	}

	public function new( )
	{
		var glb = new format.solid2.glb.Reader( File.read( "bin/full-yuri.glb" ) );
		var glbData = glb.read( );
		var gltfReader = new format.solid2.gltf.Reader( new BytesInput( glbData.chunks[0].data ) );
		var gltf = gltfReader.read( );
		var bo = new BytesOutput( );
	}
}
