# format-solid 2

3D model format Reader/Writer for Haxe Programming Language.

# Readable formats

## .obj/.mtl

Wavefront Obj

## .mqo

Metasequoia Document / Ver 1.0 and Ver 1.1

## rok

Rokkaku Daioh Format

## .x

Direct X Model File / Text Only

## .fbx

FBX Text Format 6.x ( not tested 7.x )

FBX Binary Format

## ThreeJS Format 3

ThreeJS Format 3

## ThreeJS Format 4

ThreeJS Format 4

## glTF 2.0

GL Transmission Format

# Writable formats

## .obj/.mtl

Wavefront Obj

## .mqo

Metasequoia Document / Ver 1.0 and Ver 1.1

## rok

Rokkaku Daioh Format

## .x

Direct X Model File / Text Only

## ThreeJS Format 3

ThreeJS Format 3

# Todo

## .mqz .mqx

Metasequoia New formats

# License

MIT License
